package com.serviceImpl;

import com.service.PersonService;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(value = 1)
public class StudentService implements PersonService {
}
