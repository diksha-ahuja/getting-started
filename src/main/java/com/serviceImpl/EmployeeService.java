package com.serviceImpl;

import com.service.PersonService;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Service
@Order(value = 2)
public class EmployeeService implements PersonService{
}
