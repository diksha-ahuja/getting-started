package com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.util.Locale;

@Configuration
public class LocaleConfig {
    @Value("${default.locale}")
    private String defaultLocale;

    @Bean
    public LocaleResolver localeResolver() {

        AcceptHeaderLocaleResolver resolver = new AcceptHeaderLocaleResolver();
        System.out.println("^^^^^^^^^^^^^^^"+defaultLocale);
        Locale.setDefault(new Locale(defaultLocale));
//        resolver.setDefaultLocale(new Locale(defaultLocale));
        System.out.println("*********************"+LocaleContextHolder.getLocale());
//        Locale.setDefault(new Locale(defaultLocale));
        return resolver;
    }

    @Bean
    public ResourceBundleMessageSource messageSource(){
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("i18n/messages"); // setting path for message files with initial 'messages'
//        source.setUseCodeAsDefaultMessage(true); // use message code if no message found against the code
//        source.setDefaultEncoding("UTF-8"); // use UTF-8 for encoding
        return source;
    }
}
