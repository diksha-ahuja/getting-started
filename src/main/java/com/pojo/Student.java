package com.pojo;

import javax.validation.constraints.Size;
import java.util.UUID;

public class Student {

    private String id ;

    @Size(min = 3, max = 5)
    private String name;

    public Student(){}

    public String getId() {
        return id;
    }

    public void nextId(String id) {
        this.id = id;
    }
    public void nextId() {
        this.id = UUID.randomUUID().toString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
