package com.controller;

import com.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import java.util.List;

@Controller
public class PersonController {

    @Autowired
    @Qualifier("employeeService")
    PersonService personService1;

    @Inject
    @Qualifier("studentService")
    PersonService PersonService2;

    @Autowired
    List<PersonService> personServices;

    @RequestMapping(value = {"/testing"})
    @ResponseBody
    public String test(){

        System.out.println(personServices);
        return "success";

    }

}
