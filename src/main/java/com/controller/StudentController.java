package com.controller;

import com.pojo.Student;
import com.pojo.StudentContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@RestController
public class StudentController {

    Logger logger = Logger.getLogger("student");

    @Autowired
    private StudentContainer studentContainer;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public String student(@RequestBody @Valid Student student, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
//            return messageSource.getMessage("field1", null,Locale.ITALIAN);
            return messageSource.getMessage("field1", null,LocaleContextHolder.getLocale());
        } else {
            student.nextId();
            studentContainer.setStudents(student);
            return "correct";
        }
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
    public Student getStudent(@PathVariable("id") String id) {
        logger.info("asdfasdfsdfaaaaa");
        List<Student> students=studentContainer.getStudents();
        Student resStudent=new Student();
        for(Student student: students){
            if (student.getId().equals(id)){
                resStudent=student;
                break;
            }
        }
        return resStudent;
    }

    @RequestMapping(value = "/demo", method = RequestMethod.GET)
    public String demo() {
        return messageSource.getMessage("field1", null, Locale.ITALIAN);
    }


    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public List<Student> student() {
        return studentContainer.getStudents();
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.PUT)
    public Student student(@PathVariable("id") String id) {
        List<Student> students=studentContainer.getStudents();
        Student resStudent=new Student();
        for(Student student: students){
            if (student.getId()==id){
                resStudent=student;
                break;
            }
        }
        return resStudent;
    }

    @RequestMapping(value = "/student", method = RequestMethod.DELETE)
    public List<Student> deleteStudent(@RequestParam(value="id") String id) {
        List<Student> students=studentContainer.getStudents();
        int index=-1;
        for (int i=0;i<students.size();i++){
            if (students.get(i).getId().equals(id)){
                index=i;
                break;
            }
        }
        studentContainer.getStudents().remove(index);
        return studentContainer.getStudents();
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Locale getLocale(@Autowired HttpServletRequest request){
        if(request.getHeader("Accept-Language") != null){
            System.out.println(request.getHeader("Accept-Language"));
            System.out.println(request.getHeader("accept-language"));
        }
        return LocaleContextHolder.getLocale();
    }

}
